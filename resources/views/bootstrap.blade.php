<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Hello, world!</title>
    <style>

        .lockers {
            width: 100vw;
            height: 100vh;
            display: grid;
            grid-template-columns: 10fr 1fr 10fr;
            grid-template-rows: 2fr 25fr 1fr;
            background-color: darkslategray;
            font-size: medium;
        }

        #masthead {
            display: block;
            grid-column: span 5;
            grid-row: 1;
            text-align: center;
            align-content: center;
            vertical-align: center;
            background-color: black;
            color: white;
            font-size: 175%;
            padding: 1em;
            margin-bottom: 1em;
        }

        .lockerblock {
            display: grid;
            grid-template-columns: repeat(3, 1fr);
            grid-template-rows: 1fr repeat(6, 3fr);
            grid-auto-flow: column;
            text-align: center;
            align-content: center;
            vertical-align: center;
            grid-row: 2;
        }

        #footer {
            display: block;
            grid-column: span 5;
            grid-row: 3;
            text-align: center;
            align-content: center;
            background-color: black;
            margin-top: 1em;
            color: white;
            font-size: 100%;
        }

        ul {
            padding: 1em;
            list-style: none;
        }

        li {
            display: block;
            border: 5px solid;
            text-align: center;
            vertical-align: center;
            align-content: center;
            font-size: 150%
        }

        .locker {
            text-align: center;
            vertical-align: center;
            align-content: center;
        }

        .locker-number {
            display: block;
            font-size: 125%;
            font-weight: bolder;
            font-family: Roboto, sans-serif;
            align-content: center;
        }
        .locker-note {
            display: block;
            align-content: center;
            font-size: 75%;
        }

        .available {
            background-color: palegreen;
        }
        .in-use {
            background-color: indianred;
        }
        .ending {
            background-color: coral;
        }
        .offline {
            background-color: #6b7280;
        }

        .mens {
            grid-column: 1;
            margin-left: 0.5em;
        }

        .womens {
            grid-column: 3;
            margin-right: 0.5em;
        }

        .mens li{
            border-color: dodgerblue;
        }

        .womens li {
            border-color: lightpink;
        }

        .block-header {
            grid-column: span 4;
            vertical-align: center;
            content-align: center;
            font-size: 125%;
            border-radius: 20px 20px 0 0;
        }

        #menblock-header {
            color: white;
            background-color: dodgerblue;
        }

        #womenblock-header {
            color: black;
            background-color: lightpink;
        }
    </style>
</head>
<body>
<div class="lockers">
    <div id="masthead">Locker Status</div>
    <div id="footer">Current Time</div>
    <ul class="mens lockerblock">
        <li class="block-header" id="menblock-header">
            MENS
        </li>
        <li id="1" class="locker available">
            <span class="locker-number">1</span>
            <span class="locker-note">Placeholder</span>
        </li>
        <li id="2" class="locker in-use">
            <span class="locker-number">2</span>
            <span class="locker-note">In Use</span>
        </li>
        <li id="3" class="locker ending">
            <span class="locker-number">3</span>
            <span class="locker-note">Ending</span>
        </li>
        <li id="4" class="locker offline">
            <span class="locker-number">4</span>
            <span class="locker-note">Offline</span>
        </li>
        <li id="5" class="locker available">
            <p class="locker-number">5</p>
            <span class="locker-note">Available</span>
        </li>
        <li id="6" class="locker available">
            <span class="locker-number">6</span>
            <span class="locker-note">Available</span>
        </li>
        <li id="7" class="available">
            <span class="locker-number">7</span>
            <span class="locker-note">Available</span>
        </li>
        <li id="8" class="available">
            <spanp class="locker-number">8</spanp>
            <span class="locker-note">Available</span>
        </li>
        <li id="9" class="available">
            <spanp class="locker-number">9</spanp>
            <span class="locker-note">Available</span>
        </li>
        <li id="10" class="available">
            <spanp class="locker-number">10</spanp>
            <span class="locker-note">Available</span>
        </li>
        <li id="11" class="available">
            <span class="locker-number">11</span>
            <span class="locker-note">Available</span>
        </li>
        <li id="12" class="available">
            <span class="locker-number">12</span>
            <span class="locker-note">Available</span>
        </li>
        <li id="13" class="available">
            <span class="locker-number">13</span>
            <span class="locker-note">Available</span>
        </li>
        <li id="14" class="available">
            <span class="locker-number">14</span>
            <span class="locker-note">Available</span>
        <li id="15" class="available">
            <span class="locker-number">15</span>
            <span class="locker-note">Available</span>
        <li id="16" class="available">
            <span class="locker-number">16</span>
            <span class="locker-note">Available</span>
        </li>
        <li id="17" class="available">
            <span class="locker-number">17</span>
            <span class="locker-note">Available</span>
        </li>
        <li id="18" class="available">
            <span class="locker-number">18</span>
            <span class="locker-note">Available</span>
        </li>
    </ul>

    <ul class="womens lockerblock">
        <li class="block-header" id="womenblock-header">
            WOMENS
        </li>
        <li id="19" class="locker available">
            <span class="locker-number">19</span>
            <span class="locker-note">Placeholder</span>
        </li>
        <li id="20" class="locker in-use">
            <span class="locker-number">20</span>
            <span class="locker-note">In Use</span>
        </li>
        <li id="21" class="locker ending">
            <span class="locker-number">21</span>
            <span class="locker-note">Ending</span>
        </li>
        <li id="22" class="locker offline">
            <span class="locker-number">22</span>
            <span class="locker-note">Offline</span>
        </li>
        <li id="23" class="locker available">
            <span class="locker-number">23</span>
            <span class="locker-note">Available</span>
        </li>
        <li id="24" class="locker available">
            <span class="locker-number">24</span>
            <span class="locker-note">Available</span>
        </li>
        <li id="25" class="available">
            <span class="locker-number">25</span>
            <span class="locker-note">Available</span>
        </li>
        <li id="26" class="available">
            <span class="locker-number">26</span>
            <span class="locker-note">Available</span>
        </li>
        <li id="27" class="available">
            <span class="locker-number">27</span>
            <span class="locker-note">Available</span>
        </li>
        <li id="28" class="available">
            <span class="locker-number">28</span>
            <span class="locker-note">Available</span>
        </li>
        <li id="29" class="available">
            <span class="locker-number">29</span>
            <span class="locker-note">Available</span>
        </li>
        <li id="30" class="available">
            <span class="locker-number">30</span>
            <span class="locker-note">Available</span>
        </li>
        <li id="31" class="available">
            <span class="locker-number">31</span>
            <span class="locker-note">Available</span>
        </li>
        <li id="32" class="available">
            <span class="locker-number">32</span>
            <span class="locker-note">Available</span>
        <li id="33" class="available">
            <span class="locker-number">33</span>
            <span class="locker-note">Available</span>
        <li id="34" class="available">
            <span class="locker-number">34</span>
            <span class="locker-note">Available</span>
        </li>
        <li id="35" class="available">
            <span class="locker-number">35</span>
            <span class="locker-note">Available</span>
        </li>
        <li id="36" class="available">
            <span class="locker-number">36</span>
            <span class="locker-note">Available</span>
        </li>
    </ul>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>
