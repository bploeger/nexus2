<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resource_allocations', function (Blueprint $table) {
            $table->id();
            $table->bigInteger("resource_item_id");
            $table->bigInteger('parent_id')->nullable();
            $table->bigInteger('source_id')->nullable();
            $table->string('source_type')->nullable();
            $table->char('type')->nullable();
            $table->char('reason')->nullable();
            $table->datetime('start');
            $table->datetime('end');
            $table->datetime('expires')->nullable();
            $table->json("meta")->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resource_allocations');
    }
};
