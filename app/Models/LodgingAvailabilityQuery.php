<?php

namespace App\Models;

use App\Models\Enums\LodgingType;
use Carbon\Carbon;
use DateTime;
use phpDocumentor\Reflection\Types\Collection;

class LodgingAvailabilityQuery
{
    private array $queryparams = [];
    private Carbon $start;
    private Carbon $end;
    private LodgingType $style = LodgingType::Deluxe_Cabin;

    public function __construct(Carbon|Datetime|string|int|null $from = null, int|null $nights = null, string|null $style = null)
    {
        $this->from($from ?? 'today');
        $this->nights($nights ?? 1);
        $this->style($style ?? "Deluxe");
    }

    public function from(Carbon|Datetime|string|int $start = 'today'): self
    {
        $this->start = (new Carbon($start, env('DEFAULT_TZ')))->setTime(15, 0);
        if (isset($this->end) && $this->end->isBefore($this->start))
            $this->to($this->start->copy()->addDay());
        return $this;
    }

    public function to(Carbon|Datetime|string|int $until = 'tomorrow'): self
    {
        $this->end = (new Carbon($until, env('DEFAULT_TZ')))->setTime(11, 0);
        if (isset($this->start) && $this->start->isAfter($this->end))
            $this->from($this->end->copy()->SubDay());
        return $this;
    }

    public function nights(int $nights = 1): self
    {
        $this->to($this->start->copy()->addDays($nights));
        return $this;
    }

    public function style (string $style = 'Bunkhouse'): self
    {
        $this->style = LodgingType::tryFrom($style) ?? LodgingType::Bunkhouse;
        return $this;
    }

    public function get(): LodgingAvailability
    {
        $qualifiedLodging= LodgingUnit::where('meta->style', $this->style->value)->get()->keyBy('id')->keys();
        //$bookedLodging = ResourceAllocation::where('start', "<", $this->end)->where('end', '>', $this->start)->whereIn('resource_item_id', $qualifiedLodging)->get()->keyBy('id')->keys();
        $bookedLodging = collect([3,4]);
        $availableLodging = $qualifiedLodging->diff($bookedLodging);
        return new LodgingAvailability(LodgingUnit::whereIn('id', $availableLodging)->get(), $this->start, $this->end, $this->style);
    }
}
