<?php

namespace App\Models;

use App\Models\Enums\LodgingType;
use Carbon\Carbon;
use DateTime;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Cache;

class LodgingAvailability
{
    private Collection $availableUnits;
    private Carbon $start;
    private Carbon $end;
    private LodgingType $style;
    private Carbon $notBefore;
    private Carbon $notAfter;
    private string $id;

    public function __construct (Collection $units, Carbon|Datetime|string|int $start, Carbon|Datetime|string|int $end, LodgingType $style) {
        $this->id = Str::uuid();
        $this->availableUnits = $units;
        $this->start = new Carbon($start);
        $this->end = new CArbon($end);
        $this->style = $style;
        $this->notBefore = now();
        $this->notAfter = now()->addMinutes(10);
        Cache::add($this->id, $this->output(), $this->notAfter);
    }

    private function basicArray(): array
    {
        return [
            $this->id => [
                'units' => $this->availableUnits->toArray(),
                'start' => $this->start->toIso8601String(),
                'end' => $this->end->toIso8601String(),
                'style' => $this->style->value,
                'nbf' => $this->notBefore->toIso8601String(),
                'naf' => $this->notAfter->toIso8601String()
            ]
        ];
    }

    private function asJson(): string
    {
        return json_encode($this->basicArray());
    }

    private function messageSignature(): string
    {
        return hash_hmac('sha256', $this->asJson(), env('app_key'), true);
    }

    private function output(): string
    {
        return implode(".",["ODEv1-LODGING", base64_encode($this->asJson()), base64_encode($this->messageSignature())]);
    }

    public function __toString() {
        return $this->output();
    }

    public function asArray(): array {
        return $this->basicArray();
    }




}
