<?php

namespace App\Models;

use App\Models\Enums\ResourceType;
use DateTime;
use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon\Carbon;
use Illuminate\Support\Str;

abstract class ResourceItem extends Model
{
    use SoftDeletes;

    protected $table="resource_items";
    protected array $metaArray = [];

    protected ResourceType $resource_type;

    public function isType(ResourceType $type): bool {
        return $type == $this->resource_type;
    }

    public function getType(): ResourceType {
        return $this->resource_type;
    }

    public function setType(): void {
        $this->attributes['type'] = $this->resource_type->value;
    }

    public function metaAsJson(): string {
        return json_encode($this->metaArray);
    }

    public function initializeMeta(string $meta) {
        $this->metaArray = (array) json_decode($meta);
    }

    public function setMeta(string $attr, array|string|int|null $value=null):self {
        if (in_array($attr, array_keys($this->metaArray))) {
            if (gettype($this->metaArray[$attr])==gettype($value)) {
                $this->metaArray[$attr] = $value;
            } elseif(is_null($value)) {
                $this->metaArray[$attr] = null;
            }
        }
        return $this;
    }


    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::creating(function ($item) {
            if (empty($item->getAttribute('uuid')) || !(Str::isUuid($item->getAttribute('uuid'))) ) {
                $item->uuid = Str::uuid();
            }
        });

        static::saving(function ($item) {
            $item->setType();
            $item->attributes['meta'] = $item->metaAsJson();
        });

        static::retrieved(function ($item) {
            $item->initializeMeta($item->meta);
        });

    }
}
