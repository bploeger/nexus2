<?php
namespace App\Models\Enums;

enum LodgingType: string {
    case Bunkhouse = "Bunkhouse";
    case Deluxe_Cabin = "Deluxe";
    case Signature_Cabin = "Signature";
    case Camping = "Camping";
    case Condo = "Condo";
    case House = "House";
    case Hotel = "Hotel";
}

