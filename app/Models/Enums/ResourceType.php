<?php
namespace App\Models\Enums;

enum ResourceType: string {
    case StaffAvailablity = "A";
    case Bus = "B";
    case Certification = "C";
    case Inventory = "I";
    case LodgingUnit = "L";
    case Activity = "P";
    case Room = "R";
    case Staff = "S";
    case Trailer = "T";
    case Van = "V";
    case WaterRelease = "W";
}

