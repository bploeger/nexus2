<?php
namespace App\Models\Enums;

enum Location: int {
    case Ocoee = 1;
    case Nantahala = 2;
    case Chattahoochee = 3;
    case Winterplace = 4;
    case Beech_Mountain = 5;
}

