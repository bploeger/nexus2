<?php
namespace App\Models\Enums;

enum LodgingType: string {
    case Booked = "B";
    case Hold = "H";
    case Overflow = "O";
    case Emergency = "E";

    case Cleaning = "C";
    case Maintenance = "M";

    case NotAvailable = "X";
    case Repurposed = "Y";
    case Retired = "Z";
}

