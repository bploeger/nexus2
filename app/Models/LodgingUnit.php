<?php

namespace App\Models;

use App\Models\Enums\LodgingType;
use App\Models\Enums\ResourceType;
use Carbon\Carbon;
use DateTime;

class LodgingUnit extends ResourceItem
{
    protected ResourceType $resource_type = ResourceType::LodgingUnit;
    private array $bedTypes = [
        "d" => 'Double',
        'f' => "Futon",
        'k' => "King",
        'q' => "Queen",
        't' => 'Twin',
        "b2" => "Double Bunks",
        "b3" => "Triple Bunks",
        "p2" => "Double Bunks on Sleeping Porch",
        "p3" => "Triple Bunks on Sleeping Porch"
    ];
    protected array $metaArray = [
        'min_occupancy' => 0,
        'max_occupancy' => 0,
        'style' => '',
        'amenities' => [],
        'bedrooms' => 0,
        'beds' => [
            'p3' => 0,
            'p2' => 0,
            'b3' => 0,
            'b2' => 0,
            't' => 0,
            'd' => 0,
            'q' => 0,
            'k' => 0,
            'f' => 0
        ],
        'configuration' => [
            0 => ['Room Description' => '',
                  'Beds' => [],
                  'Note']
        ],
        'online'=>[
            'name' => "",
            'available_online' => false,
            'short_description' => "",
            'long_description' => "",
            "product_image" => null,
            "featured_image"=> null,
            'rates' => [
                'default' => [
                    "rate" => 150,
                    "basis" => "per unit",
                    "description" => "per night",
                    "max_guests" => 15,
                    "min_guests" => 5,
                    "surcharges" => [
                        0 => ["description" => "TN Hotel/Motel Fee",
                              "rate" => 5.00,
                              "basis" => "night",
                              "taxable" => false],
                        1 => ["description" => "TDEC Fee",
                            "rate" => 0.10,
                            "basis" => "net",
                            "taxable" => true],
                        2 => ["description" => "Polk County, TN Sales and Use Tax",
                              "rate" => 0.0925,
                              "basis" => "net",
                              "taxable" => false],
                    ],
                ],
            ],
            "media" => [
                0 => [
                    'type'=>"img",
                    'caption' => "",
                    "links" => [
                        "xs"=> null,
                        "sm" => null,
                        "md" => null,
                        "lg" => null,
                        "xl" => null
                    ]
                ]
            ]

        ]
    ];


    public function listAmenities(): array {
        return $this->metaArray['amenities'];
    }

    public function addAmenity(array|string $amenity): self {
        if(is_array($amenity)) {
            foreach ($amenity as $item)
                $this->addAmenity($item);
        } elseif (!in_array($amenity, $this->metaArray['amenities'])) {
            $this->metaArray['amenities'][] = $amenity;
        }
        return $this;
    }

    public function removeAmenity(array|string $amenity): self {
        if(is_array($amenity)) {
            foreach ($amenity as $item)
                $this->removeAmenity($item);
        } elseif (in_array($amenity, $this->metaArray['amenities'])) {
            $this->metaArray['amenities'] =
                array_filter(
                    $this->metaArray['amenities'],
                    function($value) use ($amenity) {
                        return $value!=$amenity;
            });
        }

        return $this;
    }

    public function setStyle(LodgingType $type): self {
        $this->setMeta('style', $type->value);
        return $this;
    }

    public function minOccupancy(int $min = 1): self {
        if ($min >=1) {
            $this->setMeta('min_occupancy', $min);
        }

        return $this;
    }

    public function maxOccupancy(int $min = 1): self {
        if ($min >=1 ) {
            $this->setMeta('max_occupancy', $min);
        }

        return $this;
    }

    public function updateBeds(array $update = []): self {

        foreach ($update as $type => $quantity) {
            if (array_key_exists($type, $this->bedTypes) && is_int($quantity) && $quantity >= 0 )
                $this->metaArray['beds'][$type] = $quantity;
        }

        return $this;
    }

    public function setIdentifier(string $identifier, bool $force=false): self {
        if (empty($this->identifier) || $force) {
            $this->identifier = $identifier;
        }
        return $this;
    }

    public function setLocation(LodgingType $location) {
        $this->location = $location->value;
        return $this;
    }

    public function getLocation() : LodgingType {
        return LodgingType::tryFrom($this->location);
    }
}
