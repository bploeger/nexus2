<?php

use App\Models\Enums\LodgingType;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Route;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
const CYCLE_COUNT = 999_979;

Route::get('bootstrap', function () {
   return view('bootstrap');
});

Route::get('calendar', function () {
   $protoDay = ['date' => null, 'dayOfWeekID' => null, 'dayName' => null, 'month' => null, 'year' => null, 'dayObject' => null, 'events' => []];
   $calendar = ['start'=> null, 'end'=> null, 'weeks'=>[], 'days'=>[]];
   $start = now()->setTime(0,0, 0);
   $start = $start->subDays($start->dayOfWeek);
   $end = $start->copy()->setTime(23,59, 59)->addDays(60);
   $end = $end->addDays(6-$end->dayOfWeek);
   $weeks = $start->diffInWeeks($end)+1;
   $calendar['start'] = $start->toIso8601String();
   $calendar['end'] = $end->toIso8601String();
   $pointer = 0;
   while (count($calendar['weeks']) < $weeks) {
       $week = [];
       for ($i = 0; $i<7; $i++) {
           $date = $start->copy()->addDays($pointer);
           $day = $protoDay;
           $week[$i]['dayOfWeekID'] = $day['dayOfWeekID'] = $date->dayOfWeek;
           $week[$i]['dayName'] = $day['dayName'] = $date->dayName;
           if ($date->isBetween($start, $end)) {
               $week[$i]['date'] = $day['date'] = $date->toDateString();
               $day['month'] = $date->month;
               $day['day'] = $date->day;
               $day['year'] = $date->year;
               $day['dayObject'] = $date;

           } else {
               $week[$i]['date'] = null;
           }
           $week[$i]['index'] = $pointer;
           $calendar['days'][$pointer] = $day;
           $pointer ++;
       }
       $calendar['weeks'][] = $week;
   }
   dd($calendar);
});

Route::get('/', function () {
    $serverKey = random_bytes(SODIUM_CRYPTO_BOX_SECRETKEYBYTES);
    $serverPub = sodium_crypto_box_publickey_from_secretkey($serverKey);
    $serverPair = sodium_crypto_box_keypair_from_secretkey_and_publickey($serverKey, $serverPub);
    $start = microtime(true);
    $userSalt = Cache::get('usersalt', function() {
        $seed = sodium_bin2base64(random_bytes(48), 7);
        Cache::forever('usersalt', $seed);
        return $seed;
    });
    $keyPhrase = "It little profits that an idle king.";
    $userKey = sodium_crypto_generichash($keyPhrase,null, 48);
    $cycles = gmp_intval(($gmp = gmp_import($userKey)) % CYCLE_COUNT);
    $derivedSeed = sodium_crypto_generichash(
        $userKey,
        sodium_crypto_generichash(
            $userSalt,
            null,
            SODIUM_CRYPTO_GENERICHASH_KEYBYTES
        ),
        SODIUM_CRYPTO_SIGN_SEEDBYTES
    );
    for ($i=0;$i<CYCLE_COUNT;++$i) {
            $derivedSeed = sodium_crypto_generichash(
                ($derivedSeed . $userSalt),
                ($i < $cycles) ? $derivedSeed : "",
                SODIUM_CRYPTO_SIGN_SEEDBYTES
            );
    }

    $keypair = sodium_crypto_sign_seed_keypair($derivedSeed);

    $public = sodium_crypto_sign_publickey($keypair);
    $secret = sodium_crypto_sign_secretkey($keypair);

    $data = sodium_bin2base64(serialize(['pk' => $public, 'us' => $userSalt , 'ts' => time()]),7);
    $debug[] = "Data: " . $data;
    $signed = sodium_bin2base64(sodium_crypto_sign_detached($data, $secret),7);
    $debug[] = "Signed: " . $signed;
    $package = join(".", [$data, $signed]);
    $debug[] = "Package: " . $package;
    $sendToSVR = sodium_bin2base64(sodium_crypto_box_seal($package, $serverPub), 7);
    $debug[] = "SendtoSVR: " . $sendToSVR;
    $received = sodium_crypto_box_seal_open(sodium_base642bin($sendToSVR, 7), $serverPair);
    $debug[] = "Recieved:" . $received;
    $message = explode("." , $received);
    $debug[] = "message:" . json_encode($message);
    $payload = sodium_base642bin($message[0],7);
    $debug[] = "payload:" . $payload;
    $sig = sodium_base642bin($message[1], 7);
    $debug[] = "sig:" . $sig;
    $msg = unserialize($payload);
    $debug[] = "msg: " . join('.', $msg);
    $verify = in_array(['pk', 'us', 'ts'], array_keys($msg));
    $debug[] = "v1: " . ($verify ? "t" : "f");
    if ($verify) {
        $pk = $msg['pk'];
        $us = $msg['us'];
        $ts = $msg['ts'];
        $verify = ($us == $userSalt) && (abs(time()-$ts)<60);
    }
    $debug[] = "v2: " . ($verify ? "t" : "f");
    if ($verify) {
        $verify = sodium_crypto_sign_verify_detached($sig, $message[0], $pk);
    }
    $debug[] = "v13: " . ($verify ? "t" : "f");
    dd($debug, microtime(true) - $start);
    /*$cabin6 = new \App\Models\LodgingUnit();
    $cabin6->setIdentifier("Cabin 6")
           ->addAmenity(['private bath'])
           ->setStyle(LodgingType::Bunkhouse)
           ->updateBeds(['b2' =>10, 'b3' =>15])
           ->save();*/
    return view('bootstrap');
    dd ((new \App\Models\LodgingAvailabilityQuery("2022-06-23",2, "Bunkhouse"))->get()->asArray());
    $test1 = \App\Models\LodgingUnit::where('meta->style', 'Deluxe')->get();
    $test2 = \App\Models\LodgingUnit::whereJsonContains('meta->amenities', ['ac'])->get();
    dd($test1, $test2, \App\Models\LodgingUnit::all()->keys());
});

Route::get('allocate', function () {
    $tz = new DateTimeZone( env("DEFAULT_TZ" ) );
    $epoch = new DateTime("2010-01-01 00:00:00", $tz);
    $future = new DateTime('2199-12-31 23:59:59', $tz);
    $resources = collect([
        1 => [
            'id' => 1,
            'name' => 'Cabin 1',
            'identifer' => uniqid('ROL.'),
            'class' => 'lodging',
            'type' => 'bunkhouse',
            'meta' => [
                'min_occupancy' => 4,
                'max_occupancy' => 36,
                'style' => 'bunkhouse',
                'amenities' => [
                    'private bath',
                    'shower',
                    'ac',
                    'heat',
                    'sleeping porch',
                    'wifi',
                    'bunks'
                ],
                'bedrooms' => 1,
                'beds' => [
                    'p3' => 12,
                    'p2' => 8,
                    'b3' => 24,
                    'b2' => 16,
                    't' => 0,
                    'd' => 0,
                    'q' => 0,
                    'k' => 0,
                    'f' => 0
                ],
            ],
            'nbf' => $epoch,
            'naf' => $future,
        ],
        2 => [
            'id' => 2,
            'name' => 'Cabin2',
            'identifer' => uniqid('ROL.'),
            'class' => 'lodging',
            'type' => 'bunkhouse',
            'meta' => [
                'min_occupancy' => 4,
                'max_occupancy' => 36,
                'style' => 'bunkhouse',
                'amenities' => [
                    'private bath',
                    'shower',
                    'ac',
                    'heat',
                    'microwave',
                    'coffee maker',
                    'electronic locks',
                    'wifi',
                    'bunks'
                ],
                'bedrooms' => 1,
                'beds' => [
                    'p3' => 12,
                    'p2' => 8,
                    'b3' => 24,
                    'b2' => 16,
                    't' => 0,
                    'd' => 0,
                    'q' => 0,
                    'k' => 0,
                    'f' => 0
                ],
            ],
            'nbf' => $epoch,
            'naf' => $future,
        ],
        3 => [
            'id' => 3,
            'name' => 'Cabin 3',
            'identifer' => uniqid('ROL.'),
            'class' => 'lodging',
            'type' => 'bunkhouse',
            'meta' => [
                'min_occupancy' => 4,
                'max_occupancy' => 36,
                'amenities' => [
                    'private bath',
                    'shower',
                    'ac',
                    'heat',
                    'electronic locks',
                    'wifi',
                    'bunks'
                ],
                'bedrooms' => 1,
                'beds' => [
                    'p3' => 12,
                    'p2' => 8,
                    'b3' => 24,
                    'b2' => 16,
                    't' => 0,
                    'd' => 0,
                    'q' => 0,
                    'k' => 0,
                    'f' => 0
                ],
            ],
            'nbf' => $epoch,
            'naf' => $future,
        ],
        14 => [
            'id' => 14,
            'name' => 'Cabin 14',
            'identifer' => uniqid('ROL.'),
             'class' => 'lodging',
             'type' => 'deluxe',
             'meta' => [
                'min_occupancy' => 1,
                'max_occupancy' => 4,
                'style' => 'deluxe',
                'amenities' => [
                    0=>'private bath',
                    1=>'shower',
                    2=>'ac',
                    3=>'heat',
                    4=>'tv',
                    5=>'microwave',
                    6=>'coffee maker',
                    7=>'electronic locks',
                    8=>'deluxe cabin',
                    9=>'double'
                ],
                'bedrooms' => 1,
                'beds' => [
                    'p3' => 0,
                    'p2' => 0,
                    'b3' => 0,
                    'b2' => 0,
                    't' => 0,
                    'd' => 0,
                    'q' => 1,
                    'k' => 0,
                    'f' => 0
                ],
             ],
            'nbf' => $epoch,
            'naf' => $future,
        ],
        53 => [
            'id' => 53,
            'name' => 'Cabin 53',
            'identifier' => uniqid('ROL.'),
            'class' => 'lodging',
            'type' => 'deluxe',
            'meta' => [
                'min_occupancy' => 1,
                'max_occupancy' => 4,
                'style' => 'deluxe',
                'amenities' => [
                    'private bath',
                    'shower',
                    'ac',
                    'heat',
                    'tv',
                    'microwave',
                    'coffee maker',
                    'electronic locks',
                    'futon',
                    'queen',
                    'deluxe cabin'
                ],
                'bedrooms' => 1,
                'beds' => [
                    'p3' => 0,
                    'p2' => 0,
                    'b3' => 0,
                    'b2' => 0,
                    't' => 0,
                    'd' => 0,
                    'q' => 1,
                    'k' => 0,
                    'f' => 1
                ],
            ],
            'nbf' =>  $epoch,
            'naf' =>  $future,
        ],
    ]);

    $reserved = collect([
        1 => [
            'id' => 1,
            'resource_id' => 1,
            'type' => 'booking',
            'from' =>  new DateTime("2022-02-10 15:00:00", $tz),
            'until' => new DateTime("2022-02-18 11:00:00", $tz),
            'expires' => null,
            'active' => true,
            'is_expired' => false,
            'prior_record' => null,
            'updated_record' => null,
            'created_by' => 1
        ],
        2 => [
            'id' => 2,
            'resource_id' => 1,
            'type' => 'clean',
            'from' => new DateTime("2022-02-18 11:00:00", $tz),
            'until' =>  new DateTime("2022-02-18 15:00:00", $tz),
            'expires' => null,
            'active' => true,
            'is_expired' => false,
            'prior_record' => null,
            'updated_record' => null,
            'created_by' => 1
        ],
        3 => [
            'id' => 3,
            'resource_id' => 1,
            'type' => 'hold',
            'from' => new DateTime("2022-02-22 11:00:00", $tz),
            'until' =>  new DateTime("2022-02-28 11:00:00", $tz),
            'expires' =>  (new DateTime('now'))->add(new DateInterval('P15M')),
            'active' => true,
            'prior_record' => null,
            'updated_record' => null,
            'created_by' => 1
        ],
        4 => [
            'id' => 4,
            'resource_id' => 2,
            'type' => 'booking',
            'from' =>  new DateTime("2022-02-16 15:00:00", $tz),
            'until' =>  new DateTime("2022-02-18 11:00:00", $tz),
            'active' => true,
            'is_expired' => false,
            'prior_record' => null,
            'updated_record' => null,
            'created_by' => 1
        ],
        5 => [
            'id' => 5,
            'resource_id' => 2,
            'type' => 'clean',
            'from' =>  Carbon::create( 2022, 2, 18,11, 0, 0,  $tz )->toDateTime(),
            'until' =>  Carbon::create( 2022, 2, 18,15, 0, 0,  $tz )->toDateTime(),
            'expires' => null,
            'active' => true,
            'is_expired' => false,
            'prior_record' => null,
            'updated_record' => null,
            'created_by' => 1
        ],
    ]);

    $request = collect([
        'class' => 'lodging',
        'type' => 'deluxe',
        'start' =>  Carbon::create(2022,2,17,15, 0,0,$tz)->toDateTime(),
        'end' =>   Carbon::create(2022,2,19,11, 0, 0, $tz)->toDateTime(),
        'amenities' => ['ac']
    ]);

    $suitable = $resources->where('class', $request['class'] )
                          # ->where('type', $request['type'] )
                          ->where('nbf', "<=", $request['start'])
                          ->where('naf', ">=", $request['end']);

    $suitable = $suitable->filter(function ($value, $key) use ($request) {
                                    return count(array_intersect($request['amenities'], $value['meta']['amenities'])) == count($request['amenities']);
                               })->keys();

    $unavailable = $reserved->whereIn('resource_id', $suitable)
                            ->where('active', '=', true)
                            ->where('until', ">", $request['start'])
                            ->where('from', "<=", $request['end'])
                            ->keyBy('resource_id')
                            ->keys();

    $available = $resources->whereIn('id', $suitable->diff($unavailable));

    dd(
        $request,
        $resources,
        $suitable,
        $unavailable,
        $available);
});
